<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class ImportController extends Controller {
    
    public function readFile(Request $request) {
        
        $file = $request->file('file');
        $uploadedPath = 'uploaded_files';
        
        $file->move($uploadedPath, $file->getClientOriginalName());

        $publicPath = public_path();
        $filename = "$publicPath/$uploadedPath/" . $file->getClientOriginalName();
        
        $extension = $file->getClientOriginalExtension();        
        
        include(base_path() . '/vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Spreadsheet.php');
        include(base_path() . '/vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/IOFactory.php');
        include(base_path() . '/vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Reader/Xlsx.php');
        include(base_path() . '/vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Reader/Xls.php');
        include(base_path() . '/vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Reader/Csv.php');

        $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader(ucfirst($extension));
        /**  Advise the Reader that we only want to load cell data  **/
        $reader->setReadDataOnly(true);
        /**  Load $inputFileName to a Spreadsheet Object  **/
        $spreadsheet = $reader->load($filename)->getActiveSheet()->getRowIterator();
        $rows = [];
        $i = 0;
        foreach ($spreadsheet as $row) {            
            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(FALSE); 
            
            $cells = [];
            foreach ($cellIterator as $cell) {                
                 if ($cell->getValue()) $cells[] = $cell->getValue();                     
            }
            
            if(count($cells) > 0) $rows[] = $cells;            
            $i++;            
        }
        
        return ($rows);
    }
    
    public function importFile(Request $request, $table) {
        $data = json_decode($request->input('data_import'));
        $columnsTable = \Config::get("import.$table");
        
        // build insert query
        $query1 = "INSERT INTO $table (";
        foreach ($columnsTable as $column) {
            $query1 .= $column;
            $query1 .= (end($columnsTable) == $column)? ')' : ', ';
        }

	// Transaction begin	
       DB::beginTransaction();
        
       $totalData = count($data);
       try {
           foreach ($data as $i => $values) {
                $query2 = ' VALUES (';
                $dataInserted = [];
                foreach ($values as $value) {
                    $query2 .= "?";
                    $query2 .= (end($values) === $value)? ')' : ', ';
                    $dataInserted[] = $value;
                }                

                DB::insert($query1 . $query2, $dataInserted);
                
            }
       } catch (\Exception $ex) {
            echo 'failed';
            
            DB::rollBack();
            Log::error('Exception: Insert into table is failed, message:' . $ex->getMessage());
            return;
       }
       
       DB::commit();
       
       echo 'success';
    }
    
    public function getList($table) {
        return view('list', ['table' => $table]);
    }
}


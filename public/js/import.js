$(document).ready(function () {

    var navListItems = $('div.setup-panel div a'),
            allWells = $('.setup-content'),
            allNextBtn = $('.nextBtn');

    allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
                $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-primary').addClass('btn-default');
            $item.addClass('btn-primary');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });
    
    allNextBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='url']"),
            isValid = true;

        $(".form-group").removeClass("has-error");
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }
                
        // Upload
        if (curStepBtn === 'step-1') isValid = step1();

        // Mapping
        if (curStepBtn === 'step-2') step2();

        // import
        if (curStepBtn === 'step-3') step3();
        
        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });

    $('div.setup-panel div a.btn-primary').trigger('click');
    
    /**
     * Step 1: upload and parse
     * @returns {undefined}
     */
    function step1() {        
        $('#mapping').html('');

        var file = $('#file').val();
        var extension = file.split('.')[1];

        if (file == '') {
            var isValid = false;
            alert('Please select file first..');
            return isValid;
        }
        else if (extension != 'csv' && extension != 'xls' && extension != 'xlsx') {
            var isValid = false;
            alert('Please select csv, xls, or xlsx file..');
            return isValid;
        }
        else {
            var fd = new FormData();
            var files = $('#file')[0].files[0];
            fd.append('file',files);             

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "read-file",
                type: "POST",
                data: fd,
                processData: false,
                contentType: false,                    
                success: function(reply){
                    if (reply == 'extension_not_allowed') {
                        isValid = false;                                        
                        alert('You are only allowed select csv, xls, or xlsx file.');                                        
                    }
                    else { 
                        header = [];
                        var table = $('<table>').addClass('table table-striped');
                        var row = '<thead><tr>';
                        row += '<td><b>Header</b></td>';
                        row += '<td><b>Field</b></td>';
                        row += '</tr></thead>';                                        
                        row += '<tbody>';                                        
                        $.each(reply[0], function(i, value) {
                            row += '<tr><td>' + value + '</td>';
                            row += '<td>: <select id="field'+i+'">';
                            row += '<option value="-1"> - </option>';
                            $.each(fields, function(j, field) {
                                row += '<option value="'+j+'"' + ((i==j)? "selected":"") + '>'+field+'</option>';
                            });

                            row += '</select></td></tr>';
                            header.push(value);
                        });
                        row += '</tbody>';
                        table.append(row);

                        $('#mapping').append(table);

                        // remove first element
                        reply.shift();

                        // put data only except header
                        dataFile = reply;
                    }
                }
              });
              
              return true;
        }
    }
    // end of step-1 function //
    
    /**
     * Step 2: mapping
     * @returns {undefined}
     */
    function step2() {        
        $('#data_import').html('');
        headerFieldMapping = [];
        dataImport = [];

        // let's mapping header column with database field
        $.each(header, function(i, value) {
            headerFieldMapping.push($('#field'+i).val());
        });

        var table = $('<table>').addClass('table table-bordered');
        var row = '<thead><tr>';
        $.each(fields, function(i, field) {
            row += '<td><b>'+field+'</b></td>';
        });                        
        row += '</tr></thead>';                                        
        row += '<tbody>';

        // prepare appropriate data for db            
        $.each(dataFile, function(i, data) {
            var col = [];
            $.each(data, function(j, value) {
                if (headerFieldMapping[j] >= 0){                                    
                    col[headerFieldMapping[j]] = value;
                }
            });
            dataImport.push(col);
        }); 

        // show ready data for import
        $.each(dataImport, function(i, data) {
            row += '<tr>';
            $.each(data, function(j, value) {
                row += '<td>' + ((typeof value == 'undefined')? 'NULL' : value) + '</td>';
            });
             row += '</tr>';
        });

        row += '</tbody>';
        table.append(row);
        $('#data_import').append(table);  
    }
    // end step-2 function //
    
    /**
     * Step 3: import
     * @returns {undefined}
     */
    function step3() {        
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: 'import-file/' + table,
            type: 'POST',
            data: {
                'data_import':JSON.stringify(dataImport)
            },            
            success: function (reply) {
                
                if (reply == 'success') {
                    location.href = baseUrl + '/list/' + table;
                }
                else {
                    alert('Insert Failed! Please check your data file.');
                }
            }
        });        
    }
});
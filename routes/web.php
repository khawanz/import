<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/import/{table}', function () {
    return view('import');
});

Route::post('/import/read-file', 'ImportController@readFile');
Route::post('/import/import-file/{table}', 'ImportController@importFile');
Route::get('/list/{table}', 'ImportController@getList');
